from datetime import datetime
import glob

from pyspark.sql import SparkSession


appName = "PySpark Job"
# master = "spark://172.19.0.2:7077"
master = "local"

if __name__ == "__main__":
    """
    Run Spark Session
    """
    spark = SparkSession.builder.appName(appName).master(master).getOrCreate()

    raw_data_files = "/opt/data/20220125_103125_00086_b9kns_bucket-00000"
    read_file = glob.glob(raw_data_files)
    df_raw = spark.read.text(read_file)

    list_data = df_raw.take(
        800000
    )  # I just use 800.000 rows here because my laptop can't processing more

    save1 = []
    save2 = []
    for list_ in list_data:
        split = list_[0].split(",")
        save1.append(split[0])
        save1.append(split[1])
        save1.append(round(float(split[2]), 4))
        save1.append(round(float(split[3]), 4))
        save1.append(split[4])
        save1.append(datetime.fromtimestamp(int(split[5])/1000))
        save1.append(split[6])
        save1.append(split[7])
        save1.append(split[8])
        save1.append(split[9])
        save1.append(split[10])
        save1.append(split[11])
        save1.append(split[12])
        save1.append(split[13])
        save1.append(split[14])
        save1.append(split[15])
        save1.append(datetime.fromtimestamp(int(split[5])/1000).date())
        save2.append(save1)
        save1 = []

    columns = [
        "device_id",
        "id_type",
        "latitude",
        "longitude",
        "horizontal_accuracy",
        "timestamp",
        "ip_address",
        "device_os",
        "os_version",
        "user_agent",
        "country",
        "source_id",
        "publisher_id",
        "app_id",
        "location_context",
        "geohash",
        "date",
    ]

    df_base = spark.createDataFrame(data=save2, schema=columns)
    df_base = df_base.withColumnRenamed("timestamp", "datetime")
    del save2

    # Question 1
    df_q1 = df_base.select(["device_id", "latitude", "longitude", "datetime"])
    df_q1_duplicate =  df_q1.groupBy(df_q1.columns).count().filter("count > 1").drop('count')
    df_q1_duplicate_count = df_q1_duplicate.count()
    df_q1_duplicate.write.parquet("duplicate.parquet", mode='overwrite')
    df_parquete = spark.read.parquet("duplicate.parquet")
    print(df_parquete.show())

    # Question 2
    df_base.createOrReplaceTempView("table_base")
    number_of_unique_user_daily =  spark.sql("select date, count(distinct(device_id)) as unique_device_id from table_base group by date order by date;").show()

    # Question 3
    spark.sql(''' WITH t1 AS (

        SELECT 1 AS No, "device_id" AS Field, count(*) AS filled_data FROM table_base WHERE device_id != ""
        union(
        SELECT 2 AS No, "id_type" AS Field, count(*) AS filled_data FROM table_base WHERE id_type != "")
        union(
        SELECT 3 AS No, "latitude" AS Field, count(*) AS filled_data FROM table_base WHERE latitude IS NOT NULL)
        union(
        SELECT 4 AS No, "longitude" AS Field, count(*) AS filled_data FROM table_base WHERE longitude IS NOT NULL)
        union(
        SELECT 5 AS No, "horizontal_accuracy" AS Field, count(*) AS filled_data FROM table_base WHERE horizontal_accuracy IS NOT NULL)
        union(
        SELECT 6 AS No, "datetime" AS Field, count(*) AS filled_data FROM table_base WHERE datetime IS NOT NULL)
        union(
        SELECT 7 AS No, "ip_address" AS Field, count(*) AS filled_data FROM table_base WHERE ip_address != "")
        union(
        SELECT 8 AS No, "device_os" AS Field, count(*) AS filled_data FROM table_base WHERE device_os != "")
        union(
        SELECT 9 AS No, "os_version" AS Field, count(*) AS filled_data FROM table_base WHERE os_version != "")
        union(
        SELECT 10 AS No, "user_agent" AS Field, count(*) AS filled_data FROM table_base WHERE user_agent != "")
        union(
        SELECT 11 AS No, "country" AS Field, count(*) AS filled_data FROM table_base WHERE country != "")
        union(
        SELECT 12 AS No, "source_id" AS Field, count(*) AS filled_data FROM table_base WHERE source_id != "")
        union(
        SELECT 13 AS No, "publisher_id" AS Field, count(*) AS filled_data FROM table_base WHERE publisher_id != "")
        union(
        SELECT 14 AS No, "app_id" AS Field, count(*) AS filled_data FROM table_base WHERE app_id != "")
        union(
        SELECT 15 AS No, "location_context" AS Field, count(*) AS filled_data FROM table_base WHERE location_context != "")
        union(
        SELECT 16 AS No, "geohash" AS Field, count(*) AS filled_data FROM table_base WHERE geohash != ""))

        SELECT No, Field, ROUND((filled_data/t_total.t)*100, 2) AS Percentage_filled_data FROM t1 
        CROSS JOIN (SELECT count(*) AS t FROM table_base) t_total  
        ORDER BY No;


        ''').show()

    # Question 4
    number_of_event_per_user_per_day = spark.sql("select date, device_id, count(*) as number_of_event from table_base group by date, device_id;").show()

    # Question 5
    spark.sql(''' WITH t1 AS 
            
        (SELECT 1 AS No, "0 to 5" AS HA_Range, COUNT(*) AS Total_Events FROM table_base 
            WHERE horizontal_accuracy >= 0 AND horizontal_accuracy <= 5     
            UNION (
            SELECT 2 AS No, "6 to 10" AS HA_Range, COUNT(*) AS Total_Events FROM table_base 
            WHERE horizontal_accuracy > 5 AND horizontal_accuracy <= 10) 
            UNION (
            SELECT 3 AS No, "11 to 25" AS HA_Range, COUNT(*) AS Total_Events FROM table_base 
            WHERE horizontal_accuracy > 10 AND horizontal_accuracy <= 25)
            UNION (
            SELECT 4 AS No, "26 to 50" AS HA_Range, COUNT(*) AS Total_Events FROM table_base 
            WHERE horizontal_accuracy > 25 AND horizontal_accuracy <= 50)
            UNION (
            SELECT 5 AS No, "51 to 100" AS HA_Range, COUNT(*) AS Total_Events FROM table_base 
            WHERE horizontal_accuracy > 50 AND horizontal_accuracy <= 100)
            UNION (
            SELECT 6 AS No, "101 to 500" AS HA_Range, COUNT(*) AS Total_Events FROM table_base 
            WHERE horizontal_accuracy > 100 AND horizontal_accuracy <= 500)
            UNION (
            SELECT 7 AS No, "over 501" AS HA_Range, COUNT(*) AS Total_Events FROM table_base 
            WHERE horizontal_accuracy > 500)
            UNION (
            SELECT 8 AS No, "Total" AS HA_Range, COUNT(*) AS Total_Events FROM table_base )
            )
            
            SELECT No, HA_Range, Total_Events, Total_Events * 100 / t_total.t AS `Percentage` FROM t1
            CROSS JOIN (select count(*) AS t from table_base) t_total
            ORDER BY No;
            
            ''').show()  # Note: I don't get what "Cumulative %" column means in this context. I thought with "Percentage" column is enough

    

