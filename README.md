# quadrant test



## Getting started

The repository is intended to solve Quadrant test assignment.

By following the instructions below, you will create a necessary infrastructure that is needed to process the data.

## Environment

- Apache Maven
- Docker
- OpenJDK

## How to run

Create you spark cluster by composing the images.

    docker compose up -d

You can confirm that the cluster is running by running the following command in your browser.

    http://localhost:8080 # Spark Master WebUi
    http://localhost:8081 # Spark Worker WebUi

Run your Spark Script.

    docker exec -it spark-master ./bin/spark-submit /opt/scripts/spark.py

## Result

- Question 1

    ![alt text](pictures/question_1.jpeg "Title Text")

- Question 2

    ![alt text](pictures/question_2.jpeg "Title Text")

- Question 3

    ![alt text](pictures/question_3.jpeg "Title Text")

- Question 4

    ![alt text](pictures/question_4.jpeg "Title Text")

- Question 5

    ![alt text](pictures/question_5.jpeg "Title Text")

